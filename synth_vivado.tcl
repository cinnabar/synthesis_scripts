set src_dir [lindex $argv 0]
set dest_dir [lindex $argv 1]
set fpga_family [lindex $argv 2]
set board [lindex $argv 3]
set build_kind [lindex $argv 4]

file mkdir $dest_dir

read_verilog [glob ./$src_dir/*.v]

set_param general.maxThreads 2

if {$build_kind == "rtl"} {
    synth_design -rtl -top _module_ -name rtl
} else {
    # -mode out_of_context make the tool not use I/O buffers i.e using virtual pins
    synth_design -top _module_ -mode out_of_context -part $board
    #report_timing_summary -file $outputDir/timing_report_1.rpt 

    # Clock generation (unit is nanosecond if not set to other)
    #set_property -dict {IOSTANDARD LVCMOS33} [get_ports clk]
    create_clock -period 1 -name clk [get_ports {clk}]; 

    opt_design

    place_design -directive Default

    report_utilization -file $dest_dir/post_place_util_report.rpt
    #report_timing_summary -file $outputDir/post_place_timing_summary_2.rpt

    route_design
    #report_route_status -file $outputDir/post_route_status_3.rpt
    report_timing_summary -file $dest_dir/post_route_timing_summary.rpt

    write_checkpoint -force $dest_dir/checkpoint

    exit
}

