SHELL=/bin/bash -o pipefail

PACKAGES=$(wildcard *.tar.gz)
XILINX=$(patsubst %.tar.gz,%_xilinx/done,${PACKAGES})
ALTERA=$(patsubst %.tar.gz,%_altera/done,${PACKAGES})

all:
	@echo "use make altera, make xilinx or make both"

both: altera xilinx

altera: ${ALTERA}
xilinx: ${XILINX}

package_%: package_%/done .PHONY

XILINX_FAMILY=Kintex-7
XILINX_DEVICE=xc7k480tffg901-2

%_xilinx/done: %.tar.gz
	mkdir -p ${@D}
	cp $< ${@D}
	cd ${@D} && tar xf $<
	vivado -mode batch -source synth_vivado.tcl -tclargs ${@D} ${@D}/result \
		$(XILINX_FAMILY) $(XILINX_DEVICE) | tee ${@D}/result/stdout.log
	touch $@

%_xilinx/rtl: %.tar.gz .PHONY
	mkdir -p ${@D}
	cp $< ${@D}
	cd ${@D} && tar xf $<
	vivado -source synth_vivado.tcl -tclargs ${@D} ${@D}/result \
		$(XILINX_FAMILY) $(XILINX_DEVICE) rtl


%_altera/done: %.tar.gz
	cp ../Altera-Makefile ${@D} -r
	cp $< ${@D}/fpga
	cd ${@D}/fpga && tar xf $<
	cd ${@D}/fpga && cp metadata.toml ..
	cd ${@D}/quartus && make | tee stdout.log
	touch $@


clean:
	rm $(patsubst %/done, %, ${XILINX} ${ALTERA}) -rf

.PHONY:
